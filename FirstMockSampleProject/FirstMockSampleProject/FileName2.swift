//
//  FileName2.swift
//  FirstMockSampleProject
//
//  Created by Sagar Verma on 06/06/21.
//

import Foundation

class FileName2 {
    
    func getData() -> String {
        return "FileName2"
    }
}
